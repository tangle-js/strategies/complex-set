const get = require('lodash.get')
const setWith = require('lodash.setwith')
const isEqual = require('lodash.isequal')
const isEmpty = require('lodash.isempty')
const mergeWith = require('lodash.mergewith')
const Validator = require('is-my-json-valid')

const IDENTITY = {}

module.exports = function ComplexSetRule (idPattern) {
  if (!idPattern) idPattern = '^.+$'
  const schema = {
    type: 'object',
    patternProperties: {
      [idPattern]: {
        type: 'object',
        patternProperties: {
          '^\\d+$': { type: 'integer' }
        },
        additionalProperties: false
      }
    },
    additionalProperties: false
  }

  /*
   gets the new state from the current state + an input (change)
  */
  function mapFromInput (input, currentTips) {
    if (input === null || input === undefined) return {}
    if (typeof input !== 'object') throw new CustomError('complexSet must be an Object', currentTips, input)
    if (!Object.keys(input).length) throw new CustomError('complexSet must be an Object of shape { add: [Object], remove: [Object] }', currentTips, input)

    const { add = [], remove = [] } = input

    if (!add || !Array.isArray(add)) throw new CustomError('complexSet.add must be an Array of Objects', currentTips, input)
    if (!remove || !Array.isArray(remove)) throw new CustomError('complexSet.remove must be an Array of Objects', currentTips, input)

    if (add.length === 0 && remove.length === 0) return {}

    const addErrors = !add.every(isValidEntry)
    const rmErrors = !remove.every(isValidEntry)
    if (addErrors || rmErrors) throw new CustomError('complexSet.add or complexSet.remove contains invalid input', currentTips, input)

    // check to see if there exists an item that is in both arrays
    const intersection = add.some(d => {
      return remove.some(rm => {
        if (isEqual(rm, d)) return true
        return rm.id === d.id
      })
    })

    if (intersection) throw new CustomError('complexSet.add and complexSet.remove have an item with the same id', currentTips, input)

    const change = {}

    add.forEach(({ id, seq }) => {
      const current = currentTips
        .map(currentT => get(currentT, [id, seq]) || 0)
        .reduce((a, b) => a + b, 0)
      if (current <= 0) {
        setWith(change, [id, seq], (-1 * current) + 1, Object)
      }
    })

    remove.forEach(({ id, seq }) => {
      const current = currentTips
        .map(currentT => get(currentT, [id, seq]) || 0)
        .reduce((a, b) => a + b, 0)
      if (current >= 0) {
        setWith(change, [id, seq], (-1 * current) - 1, Object)
      }
    })

    return change
  }

  /*
    transformations are of form:
    {
      String: { Integer: Integer }
    }
    e.g.
    {
      id: { seq: state }
    }
  */

  function mapToOutput (T) {
    const res = {}

    Object.entries(T).forEach(([id, value]) => {
      const events = Object.entries(value)
        .map(([seq, state]) => [Number(seq), state])
        .sort((a, b) => a[0] - b[0])

      // keep track of if we're currently looking for the next "add" or "remove"
      let isAdd = true
      let interval = new Interval()

      const history = []

      while (events.length) {
        const [seq, state] = events.shift()
        // must alternate between add (start) and remove (end) for intervals
        if (isAdd && state > 0) {
          interval.start = seq
          // set to false to trigger the check for remove
          isAdd = false
        } else if (!isAdd && state <= 0) {
          interval.end = seq
          // set to true to trigger the check for add
          isAdd = true

          // means theres a complete interval with start+end and its added to the final res
          history.push(interval)

          // reset the values
          interval = new Interval()
        }
      }

      // we only include incomplete intervals which have a "start" sequence
      if (interval.start !== null) history.push(interval)

      // to ensure res[id] isnt an empty array
      if (history.length) res[id] = history
    })

    return res
  }

  function concat (a, b) {
    const res = mergeWith({}, a, b, (objValue, srcValue) => {
      if (typeof objValue !== 'number') return
      return objValue + srcValue
    })

    Object.entries(res).forEach(([key, val]) => {
      Object.entries(val).forEach(([seq, state]) => {
        if (state === 0) {
          delete res[key][seq]
          if (isEmpty(res[key])) delete res[key]
        }
      })
    })

    return res
  }

  function isConflict () {
    // Do we want to have a conflict if the sign is different in each branch?
    // Currently if the merge is identity it just adds them together and we get whatever.
    return false
  }

  function isValidMerge () {
    return true
  }

  function merge (graph, mergeNode, field) {
    // Build a set of the keys in the history of mergeNode
    const keyHistory = new Set(mergeNode.previous)
    for (const prevKey of mergeNode.previous) {
      for (const historyKey of graph.getHistory(prevKey)) {
        keyHistory.add(historyKey)
      }
    }

    // Concat all of the fields in the history together
    const concatHistory = Array.from(keyHistory)
      .map((key) => {
        if (field in graph.getNode(key).data) return graph.getNode(key).data[field]
        else return IDENTITY
      })
      .reduce((a, b) => concat(a, b))

    // Concat the merge node field as well.
    const mergeNodeField = field in mergeNode.data ? mergeNode.data[field] : IDENTITY
    return concat(concatHistory, mergeNodeField)
  }

  return {
    mapFromInput,
    mapToOutput,
    schema,
    isValid: Validator(schema),
    identity: () => IDENTITY,
    concat,
    isConflict,
    isValidMerge,
    merge,

    /* LEGACY */

    humanToTransform: mapFromInput,
    reify: mapToOutput
  }
}

function Interval () {
  return { start: null, end: null }
}

function CustomError (msg, input, currentTips) {
  const errorMsg = `${FormatMsg(msg)}. input: ${FormatMsg(input)}, currentTips: ${FormatMsg(currentTips)}`

  return Error(errorMsg)
}

function FormatMsg (msg) {
  return JSON.stringify(msg, null, 2)
}

function isValidEntry (entry) {
  if (typeof entry !== 'object') return false
  if (Object.keys(entry).length !== 2) return false

  return (
    typeof entry.id === 'string' &&
    typeof entry.seq === 'number'
  )
}
