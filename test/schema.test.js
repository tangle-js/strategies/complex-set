const test = require('tape')
const ComplexSet = require('../')

test('schema', t => {
  const { identity, isValid } = ComplexSet()

  t.true(isValid(identity()), 'identity is valid')

  const T1 = {
    '@cherese': { 50: 1 },
    '@mix': { 0: 1, 100: -1 }
  }
  t.true(isValid(T1), 'T1 is valid')
  if (isValid.error) console.log(isValid.error)

  const T2 = {
    '@cherese': { dog: 2 },
    '@mix': { 50: 1, 100: -1 }
  }
  t.false(isValid(T2), 'seq cannot be a string')

  const T3 = {
    '@cherese': { '-50': 1 }
  }
  t.false(isValid(T3), 'seq must be a whole number')

  const T4 = {
    '@cherese': { 50: null }
  }
  t.false(isValid(T4), 'state must be an integer')
  const T5 = {
    '@cherese': { 50: 1.2 }
  }
  t.false(isValid(T5), 'state must be an integer')

  t.end()
})

test('schema (custom idPattern)', t => {
  const { identity, isValid } = ComplexSet('^(\\*|@[\\w]+)$')

  t.true(isValid(identity()), 'identity is valid')

  const T1 = {
    '*': { 50: 1 }
  }
  t.true(isValid(T1), 'id * is valid')

  const T2 = {
    '@mix': { 50: 1, 100: -1 }
  }
  t.true(isValid(T2), 'id @mix is valid')

  const T3 = {
    '*': { 50: 1 },
    '@mix': { 50: 1, 100: -1 }
  }
  t.true(isValid(T3), 'id * and @mix valid')

  const T4 = {
    cherese: { 50: 1 }
  }
  t.false(isValid(T4), 'id "cherese" is invalid')

  const T5 = {
    '@cherese e': { 50: 1 }
  }
  t.false(isValid(T5), 'id "@cherese e" is invalid')

  t.end()
})
