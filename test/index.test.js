const test = require('tape')
const Strategy = require('@tangle/strategy')

const ComplexSetRule = require('../')

test('strategy.isValid', t => {
  const complex = ComplexSetRule()
  const isValid = Strategy.isValid(complex)
  t.true(isValid, 'complex-set is a valid strategy')
  if (!isValid) console.log(Strategy.isValid.error)

  t.end()
})

test('complex-set', t => {
  const {
    isValid,
    reify,
    concat,
    identity
  } = ComplexSetRule()

  const chereseFeedId = 'cherese'
  const mixFeedId = '@mixVv6k5mnWKkJTgCIpqOsA4JeJd9Oz2gmv6rojQeXU=.ed25519'
  const benFeedId = 'BEN_ID'

  const timestamp = Number(new Date())

  // isValid /////////////////////////////

  const Ts = [
    {
      USER1234513: { 1: 1, 10000000: -1 }
    },
    { // simple add
      [chereseFeedId]: { 100: 1 }
    },
    {
      [chereseFeedId]: { 100: 0 }
    },
    { // simple remove
      [chereseFeedId]: { 500: -1 }
    },
    {
      [mixFeedId]: { 100: 1 },
      [benFeedId]: { 200: 2 }
    },
    { // order-invariance
      [mixFeedId]: { 100: 2 },
      [benFeedId]: { 200: 1 }
    },

    {
      [mixFeedId]: { 100: 0 },
      [benFeedId]: { 200: 2 }
    },
    {
      [mixFeedId]: { 100: 1, 200: 1 } // idempotent
    },
    {
      [mixFeedId]: { 100: 1, 200: -1, 300: 1 }
    },
    {
      [mixFeedId]: { 100: 1, 200: -1, 150: -1 }
    },
    {
      USER_217: { [timestamp]: 1 }
    },
    identity(), // {}
    {
      [chereseFeedId]: { 0: 1 }
    }
  ]

  Ts.forEach(T => {
    t.true(isValid(T), `isValid : ${JSON.stringify(T)}`)
  })

  const notTs = [
    'dog',
    undefined,
    null,
    true,
    {
      [chereseFeedId]: { 200: 2.5 }
    },
    {
      [chereseFeedId]: { 200: 'duck' }
    },
    {
      [chereseFeedId]: { 200: null }
    },
    {
      [chereseFeedId]: { 200: true }
    }

    /* JSON validation passed these two ... */
    // {
    //   [chereseFeedId]: { 200: 2, 300: undefined }
    // },
    // {
    //   [chereseFeedId]: { 200: undefined }
    // }
  ]

  notTs.forEach(T => {
    t.false(isValid(T), `!isValid : ${JSON.stringify(T)}`)
  })

  // reify

  const expectedReify = [
    { USER1234513: [{ start: 1, end: 10000000 }] }, // 'USER1234513': { 0: 1, 10000000: -1 }
    { [chereseFeedId]: [{ start: 100, end: null }] }, // { [chereseFeedId]: { 100: 1 } },
    {}, // { [chereseFeedId]: { 100: 0 } },
    {}, // { [chereseFeedId]: { 500: -1 } },
    {
      [mixFeedId]: [{ start: 100, end: null }],
      [benFeedId]: [{ start: 200, end: null }]
    }, // { [mixFeedId]: { 100: 1 }, [benFeedId]: { 200: 2 } }
    {
      [mixFeedId]: [{ start: 100, end: null }],
      [benFeedId]: [{ start: 200, end: null }]
    }, // { [mixFeedId]: { 100: 2 }, [benFeedId]: { 200: 1 } } order-invariance
    {
      [benFeedId]: [{ start: 200, end: null }]
    }, // { [mixFeedId]: { 100: 0 }, [benFeedId]: { 200: 2 } },
    {
      [mixFeedId]: [{ start: 100, end: null }]
    }, // [mixFeedId]: { 100: 1, 200: 1 }
    {
      [mixFeedId]: [{ start: 100, end: 200 }, { start: 300, end: null }]
    }, // [mixFeedId]: { 100: 1, 200: -1, 300: 1 }
    {
      [mixFeedId]: [{ start: 100, end: 150 }]
    }, // [mixFeedId]: { 100: 1, 200: -1, 150: -1 }
    {
      USER_217: [{ start: timestamp, end: null }]
      // 'USER_217': { [timestamp]: 1 }
    },
    {}, // identity()
    { [chereseFeedId]: [{ start: 0, end: null }] }
  ]

  Ts.forEach((T, i) => {
    t.deepEqual(reify(T), expectedReify[i], `reify :  ${JSON.stringify(T)}`)
  })

  // concat

  t.deepEqual(
    concat(
      { [chereseFeedId]: { 100: 1 } },
      { [chereseFeedId]: { 200: -1 } }
    ),
    { [chereseFeedId]: { 100: 1, 200: -1 } },
    'concat(a,b)=c'
  )

  t.deepEqual(
    concat(
      { [chereseFeedId]: { 200: -1 } },
      { [chereseFeedId]: { 100: 1 } }
    ),
    { [chereseFeedId]: { 100: 1, 200: -1 } },
    'concat(b,a)=c'
  )

  t.deepEqual(
    concat(
      { [chereseFeedId]: { 100: 1 } },
      { [chereseFeedId]: { 100: 2 } }
    ),
    { [chereseFeedId]: { 100: 3 } },
    'concat(a=1,b=2)=c=3'
  )

  t.deepEqual(
    concat(
      { [chereseFeedId]: { 100: 2 } },
      { [chereseFeedId]: { 100: 1 } }
    ),
    { [chereseFeedId]: { 100: 3 } },
    'concat(b=2,a=1)=c=3'
  )

  t.deepEqual(
    concat(
      { [chereseFeedId]: { 100: 2 } },
      { [chereseFeedId]: { 100: -2 } }
    ),
    identity(),
    'concat(a,-a)=identity'
  )

  t.deepEqual(
    concat(
      { [chereseFeedId]: { 100: 2 } },
      identity()
    ),
    { [chereseFeedId]: { 100: 2 } },
    'concat(a,identity)=a'
  )

  t.deepEqual(
    concat(
      identity(),
      { [chereseFeedId]: { 100: 2 } }
    ),
    { [chereseFeedId]: { 100: 2 } },
    'concat(identity,a)=a'
  )

  const A = { [chereseFeedId]: { 100: 2 } }
  const B = { [chereseFeedId]: { 200: -1 } }
  const C = { [chereseFeedId]: { 300: 1 }, [benFeedId]: { 400: 1 } }

  t.deepEqual(
    concat(concat(A, B), C),
    concat(A, concat(B, C)),
    'associativity'
  )

  t.end()
})
