const test = require('tape')
const Strategy = require('../')
const tangle = require('@tangle/test')

const {
  identity,
  isConflict,
  isValidMerge,
  merge
} = Strategy()

test('merging', t => {
  const chereseFeedId = 'cherese'
  const mixFeedId = '@mixVv6k5mnWKkJTgCIpqOsA4JeJd9Oz2gmv6rojQeXU=.ed25519'

  const buildGraph = (mermaid) => tangle.buildGraph(mermaid, { dataField: 'complex' })
  const mergeGraph = buildGraph(`
     A-->B->D
     A-->C->D
   `)
  const mergeNode = mergeGraph.getNode('D')
  t.equal(isConflict(mergeGraph, ['B', 'C']), false, 'no conflict')
  t.equal(isValidMerge(mergeGraph, mergeNode), true, 'merge is always fine')
  // merge

  t.deepEqual(merge(mergeGraph, mergeNode, 'complex'), identity(), ' identity merge')

  const A = mergeGraph.getNode('A')
  A.data.complex[chereseFeedId] = { 100: 1 }
  t.deepEqual(merge(mergeGraph, mergeNode, 'complex'),
    { [chereseFeedId]: { 100: 1 } }, 'merge with root value')

  const B = mergeGraph.getNode('B')
  B.data.complex[chereseFeedId] = { 100: 1 }
  t.deepEqual(merge(mergeGraph, mergeNode, 'complex'),
    { [chereseFeedId]: { 100: 2 } }, 'merge one branch different feedIds')

  const C = mergeGraph.getNode('C')
  C.data.complex[chereseFeedId] = { 200: 2 }
  t.deepEqual(merge(mergeGraph, mergeNode, 'complex'),
    { [chereseFeedId]: { 100: 2, 200: 2 } }, 'merge two branches')

  mergeNode.data.complex[chereseFeedId] = { 100: -1 }
  t.deepEqual(merge(mergeGraph, mergeNode, 'complex'),
    { [chereseFeedId]: { 100: 1, 200: 2 } }, 'merge all nodes')

  // Adding mixFeedId to above graph
  B.data.complex[mixFeedId] = { 100: 1 }
  t.deepEqual(merge(mergeGraph, mergeNode, 'complex'),
    { [chereseFeedId]: { 100: 1, 200: 2 }, [mixFeedId]: { 100: 1 } }, 'multiple feedIds')
  t.end()
})
