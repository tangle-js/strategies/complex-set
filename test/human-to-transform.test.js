const test = require('tape')
const strategy = require('..')()

/* test('transformComplexSet', async t => {
  const apiSets = [
    {},
    { add: null },
    { add: undefined },
    { dog: [] },
    { add: [] },
    { add: [], remove: [] }, // NOTE: remove should be ignored
    // COMPLEX SETS
    { // add single item
      add: [{ id: '@cherese', seq: 200 }]
    },
    { // add + remove the same item
      add: [{ id: '@cherese', seq: 200 }],
      remove: [{ id: '@cherese', seq: 200 }] // should be ignored
    },
    { // add multiple
      add: [
        { id: '@ben', seq: 400 },
        { id: '@mix', seq: 500 }
      ]
    },
    { // add multiple reversed order
      add: [
        { id: '@mix', seq: 500 },
        { id: '@ben', seq: 400 }
      ]
    }
  ]

  const expectedTransform = [
    {},
    {},
    {},
    {},
    {}, // sets that have { add: [] } return empty T
    {},
    // COMPLEX SETS - authors
    { '@cherese': { 200: 1 } },
    { '@cherese': { 200: 1 } }, // remove operation was ignored
    {
      '@ben': { 400: 1 },
      '@mix': { 500: 1 }
    },
    {
      '@ben': { 400: 1 },
      '@mix': { 500: 1 }
    },
    {}
  ]

  apiSets.forEach((input, i) => {
    t.deepEqual(
      initialTransformationComplexSet(input),
      expectedTransform[i],
      `transformSet : ${JSON.stringify(input)}`
    )
  })

  t.end()
}) */

test('humanToTransform (valid)', t => {
  const initialStates = [
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    { '@cherese': { 200: 1 } },
    { '@cherese': { 200: 1 } },
    { '@cherese': { 200: 1 } },
    { '@cherese': { 200: 1, 500: -1 } },
    { '@cherese': { 200: -1 } },
    {}
  ]
  const validInputs = [
    null,
    undefined,
    { add: [], remove: [] },
    { add: [] },
    { remove: [] },
    {
      add: [{ id: 'cherese', seq: 1 }]
      // remove: [{ id: 'mix', seq: 100 }]
    },
    {
      add: [{ id: '@cherese', seq: 100 }]
    },
    {
      add: [{ id: '@mixmix', seq: 200 }]
    },
    {
      remove: [{ id: '@cherese', seq: 400 }]
    },
    {
      add: [{ id: '@cherese', seq: 200 }]
    },
    {
      remove: [{ id: '@cherese', seq: 500 }]
    },
    {
      add: [{ id: '@cherese', seq: 200 }]
    },
    {
      add: [{ id: '@cherese', seq: 0 }]
    }
  ]

  const expectedOutputs = [
    {},
    {},
    {},
    {},
    {},
    { cherese: { 1: 1 } },
    { '@cherese': { 100: 1 } },
    { '@mixmix': { 200: 1 } },
    { '@cherese': { 400: -1 } },
    { },
    { },
    { '@cherese': { 200: 2 } },
    { '@cherese': { 0: 1 } }
  ]

  validInputs.forEach((input, i) => {
    t.deepEqual(
      strategy.humanToTransform(input, [initialStates[i]]),
      expectedOutputs[i],
      `${JSON.stringify(input)} => ${JSON.stringify(expectedOutputs[i])}`
    )
  })

  t.end()
})

test('humanToTransform (invalid)', t => {
  const invalidInputs = [
    'dog',
    false,
    true,
    {},
    { add: null },
    { add: 'dog' },
    { add: false },
    { add: true },
    { add: 1234 },
    { add: [{}] },
    { add: [{ one: '1', two: '2', three: '3' }] },
    { add: [{ id: undefined, seq: 1 }] },
    { add: [{ id: '@someId', seq: undefined }] },
    { add: [{ id: undefined, seq: undefined }] },
    { add: [{ id: '@someId', seqq: 1 }] }, // typo
    { remove: null },
    { remove: 'cat' },
    { remove: false },
    { remove: true },
    { remove: 1234 },
    { remove: [{}] },
    { remove: [{ one: '1', two: '2', three: '3' }] },
    { remove: [{ id: undefined, seq: 1 }] },
    { remove: [{ id: '@someId', seq: undefined }] },
    { remove: [{ id: undefined, seq: undefined }] },
    {
      add: [{ id: 'cherese', seq: 1 }],
      remove: [{ id: 'cherese', seq: 100 }] // 26
    }
  ]

  const expectedErr = [
    'complexSet must be an Object',
    'complexSet must be an Object',
    'complexSet must be an Object',
    'complexSet must be an Object of shape',
    'complexSet.add must be an Array of Objects',
    'complexSet.add must be an Array of Objects',
    'complexSet.add must be an Array of Objects',
    'complexSet.add must be an Array of Objects',
    'complexSet.add must be an Array of Objects',
    'complexSet.add or complexSet.remove contains invalid input',
    'complexSet.add or complexSet.remove contains invalid input',
    'complexSet.add or complexSet.remove contains invalid input',
    'complexSet.add or complexSet.remove contains invalid input',
    'complexSet.add or complexSet.remove contains invalid input',
    'complexSet.add or complexSet.remove contains invalid input',
    'complexSet.remove must be an Array of Objects',
    'complexSet.remove must be an Array of Objects',
    'complexSet.remove must be an Array of Objects',
    'complexSet.remove must be an Array of Objects',
    'complexSet.remove must be an Array of Objects',
    'complexSet.add or complexSet.remove contains invalid input',
    'complexSet.add or complexSet.remove contains invalid input',
    'complexSet.add or complexSet.remove contains invalid input',
    'complexSet.add or complexSet.remove contains invalid input',
    'complexSet.add or complexSet.remove contains invalid input',
    'complexSet.add and complexSet.remove have an item with the same id'// 26
  ]

  invalidInputs.forEach((input, i) => {
    t.throws(
      () => strategy.humanToTransform(input, [{}]),
      RegExp(expectedErr[i]),
      `${JSON.stringify(input)} => ${expectedErr[i]}`
    )
  })

  t.end()
})

test('reify', t => {
  const T = {
    // an example set where each @user was permitted at different intervals
    '@cherese': {
      200: 1, // seq=200, state=1=add
      1000: -1 // seq=1000, state=-1=remove
    },
    '@ben': {
      1000: 1, // seq=1000, state=1=add
      1100: 2, // seq=1100, state=2=add
      2000: -1 // seq=2000, state=-1=remove
    },
    '@mix': {
      300: -1 // seq=300, state=-1=remove
    }
  }

  const expected = {
    '@cherese': [{ start: 200, end: 1000 }],
    '@ben': [{ start: 1000, end: 2000 }] // note how 1100 wasnt added
  }

  t.deepEqual(strategy.reify(T), expected)

  t.end()
})

test('merge (valid)', t => {
  const initialStates = [
    [{}, {}],
    [{}, {}],
    [{}, {}],
    [{}, {}],
    [{}, {}],
    [{}, {}],
    [{}, {}],
    [{ '@cherese': { 200: 1 } }, {}],
    [{ '@cherese': { 200: 1 } }, {}],
    [{ '@cherese': { 200: 1 } }, {}],
    [{ '@cherese': { 200: 1, 500: -1 } }, {}],
    [{ '@cherese': { 200: -1 } }, {}],
    [{}, {}],
    // New test cases:
    [{ '@colin': { 200: 4 } }, { '@colin': { 200: -2 } }],
    [{ '@colin': { 200: 4 } }, { '@colin': { 200: -2 } }],
    [{ '@colin': { 200: 4 } }, { '@colin': { 200: -2 } }]

  ]
  const validInputs = [
    null,
    undefined,
    { add: [], remove: [] },
    { add: [] },
    { remove: [] },
    {
      add: [{ id: 'cherese', seq: 1 }]
      // remove: [{ id: 'mix', seq: 100 }]
    },
    {
      add: [{ id: '@cherese', seq: 100 }]
    },
    {
      add: [{ id: '@mixmix', seq: 200 }]
    },
    {
      remove: [{ id: '@cherese', seq: 400 }]
    },
    {
      add: [{ id: '@cherese', seq: 200 }]
    },
    {
      remove: [{ id: '@cherese', seq: 500 }]
    },
    {
      add: [{ id: '@cherese', seq: 200 }]
    },
    {
      add: [{ id: '@cherese', seq: 0 }]
    },
    // New test cases:
    { add: [] },
    {
      add: [{ id: '@colin', seq: 200 }]
    },
    {
      remove: [{ id: '@colin', seq: 200 }]
    }
  ]

  const expectedOutputs = [
    {},
    {},
    {},
    {},
    {},
    { cherese: { 1: 1 } },
    { '@cherese': { 100: 1 } },
    { '@mixmix': { 200: 1 } },
    { '@cherese': { 400: -1 } },
    { },
    { },
    { '@cherese': { 200: 2 } },
    { '@cherese': { 0: 1 } },
    // New test cases:
    {},
    {},
    { '@colin': { 200: -3 } }
  ]

  validInputs.forEach((input, i) => {
    t.deepEqual(
      strategy.humanToTransform(input, initialStates[i]),
      expectedOutputs[i],
      `${JSON.stringify(input)} => ${JSON.stringify(expectedOutputs[i])}`
    )
  })
  t.end()
})
